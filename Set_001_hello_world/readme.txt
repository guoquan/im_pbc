Set 001 Hello World
===
Introduction
---
First things first. Like any senior programmer may had done, write a 'Hello world' program as your first program to begin with.

Instructions
---
Simply print "hello, world" (without capital letters or exclamation mark) to follow Kernighan's paradigm, but make sure you are using C++. Reference any textbook in C++.

If it is too simple for you, try to demonstrate anything you may know about C++ in the program, but always keep in mind that the program should do nothing other than printing the phrase "hello, world".

Postscript
---
Have a good time and we will share what we have after group meeting next week.